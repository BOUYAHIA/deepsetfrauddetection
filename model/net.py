"""Defines the neural network, losss function and metrics"""

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from sklearn.metrics import classification_report, accuracy_score
from typing import Union
import math
from sklearn.metrics import confusion_matrix

from torch import FloatTensor
from torch.autograd import Variable

#NetIO = Union[FloatTensor, Variable]


class InvariantModel(nn.Module):
    def __init__(self, params, phi: nn.Module, rho: nn.Module):
        super().__init__()
        self.phi = phi
        self.rho = rho
        self.params = params

    def forward(self, x, sizes_batch):
        # compute the representation for each data point
        nb_exemple = x.shape[0]
        x = self.phi.forward(x, sizes_batch)

        # compute the output
        out = self.rho.forward(x)

        #out = out.reshape(self.params.batch_size)

        out = out.reshape(nb_exemple)

        return out


class Phi(nn.Module):
    def __init__(self, params):
        super().__init__()
        self.params = params

        self.fc1 = nn.Linear(self.params.input_size, self.params.hidden_dim)
        self.bn1 = nn.BatchNorm1d(num_features=self.params.hidden_dim)
        """
        self.fc2 = nn.Linear(self.params.hidden_dim, self.params.hidden_dim)
        self.bn2 = nn.BatchNorm1d(num_features=self.params.hidden_dim)
        self.fc3 = nn.Linear(self.params.hidden_dim, self.params.hidden_dim)
        self.bn3 = nn.BatchNorm1d(num_features=self.params.hidden_dim)
        self.fc4 = nn.Linear(self.params.hidden_dim, self.params.hidden_dim)
        self.bn4 = nn.BatchNorm1d(num_features=self.params.hidden_dim)
        """
        self.fc2 = nn.Linear(self.params.hidden_dim, self.params.projection_dim)

    def forward(self, x, sizes_batch) :
        projection = torch.zeros(x.shape[0],self.params.projection_dim)

        for i in range(x.shape[1]) :
            out = x[:,i,:]
            out = F.relu(self.bn1(self.fc1(out)))
            """
            out = F.relu(self.bn2(self.fc2(out)))
            out = F.relu(self.bn3(self.fc3(out)))
            out = F.relu(self.bn4(self.fc4(out)))
            """
            out = self.fc2(out)

            projection += out

        list_of_sizes = list()

        for i in range (self.params.projection_dim) :
            list_of_sizes.append(sizes_batch)

        sizes_batch = torch.FloatTensor(list_of_sizes)

        sizes_batch = torch.transpose(sizes_batch, 0, 1)

        return projection / sizes_batch


class Rho(nn.Module):
    def __init__(self, params):
        super().__init__()
        self.params = params
        self.bn5 = nn.BatchNorm1d(num_features=self.params.projection_dim)
        self.fc6 = nn.Linear(self.params.projection_dim, self.params.hidden_dim)
        self.bn6 = nn.BatchNorm1d(num_features=self.params.hidden_dim)
        #self.fc7 = nn.Linear(self.params.hidden_dim, self.params.hidden_dim)
        #self.bn7 = nn.BatchNorm1d(num_features=self.params.hidden_dim)
        self.fc7 = nn.Linear(self.params.hidden_dim, self.params.output_size)

    def forward(self, x):
        x = self.bn5(x)
        x = F.relu(self.bn6(self.fc6(x)))
        #x = F.relu(self.bn7(self.fc7(x)))
        x = torch.sigmoid(self.fc7(x))
        return x

def Confusion_matrix(outputs, labels):

    print(max(outputs))
    print(labels)

    labels = [int(p) for p in labels]
    outputs = [int(p) for p in outputs]

    return confusion_matrix( labels, outputs)

def accuracy(outputs, labels):
    outputs = [int(x) for x in outputs ]
    labels = [int(x) for x in labels]
    outputs = accuracy_score(outputs, labels)

    return outputs


# maintain all metrics required in this dictionary- these are used in the training and evaluation loops
metrics = {
    'accuracy': accuracy,
    'confusion_matrix' : Confusion_matrix,
}
