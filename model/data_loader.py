import random
import numpy as np
import os
import sys
import ast

import torch
from torch.autograd import Variable
from torch import FloatTensor
from torch.utils.data.dataset import Dataset
from typing import Tuple
from itertools import repeat

import utils

class DataLoader_1(Dataset):
    def __init__(self, path_dir: str, dataset_len: int):
        self.path_data_file = os.path.join(path_dir, 'sets.txt')
        self.path_label_file = os.path.join(path_dir, 'labels.txt')
        self.dataset_len = dataset_len

        self.data = list()
        self.label = list()

        with open(self.path_data_file) as f:
            for i, line in enumerate(f):
                self.data.append(line.strip().split(';'))

        with open(self.path_label_file) as f:
            for i, line in enumerate(f):
                self.label.append(line)

    def __len__(self) -> int:
        return self.dataset_len

    def __getitem__(self, item: int) -> Tuple[FloatTensor, FloatTensor]:
        liste = [torch.FloatTensor(ast.literal_eval(x)) for x in self.data[item]]

        return torch.stack(liste, dim=0), torch.FloatTensor([int(self.label[item])])





class DataLoader(Dataset):
    """
    Handles all aspects of the data. Stores the dataset_params, vocabulary and tags with their mappings to indices.
    """
    def __init__(self, data_dir, params):

        # loading dataset_params
        json_path = os.path.join(data_dir, 'dataset_params.json')

        # adding dataset parameters to param
        params.update(json_path)

    def load_sets_labels(self, sets_file, labels_file, d):
        """
        Loads sentences and labels from their corresponding files. Maps tokens and tags to their indices and stores
        them in the provided dict d.

        Args:
            sentences_file: (string) file with sentences with tokens space-separated
            labels_file: (string) file with NER tags for the sentences in labels_file
            d: (dict) a dictionary in which the loaded data is stored
        """

        sets = []
        labels = []

        with open(sets_file) as f:
            for i, line in enumerate(f):
                liste = [ast.literal_eval(x) for x in line.strip().split(';')]
                sets.append(liste)

        with open(labels_file) as f:
            for i, line in enumerate(f):
                labels.append(int(line))


                # checks to ensure there is a tag for each token
        assert len(labels) == len(sets)

        # storing sentences and labels in dict d
        d['data'] = sets
        d['labels'] = labels
        d['size'] = len(sets)

    def load_data(self, types, data_dir):
        """
        Loads the data for each type in types from data_dir.

        Args:
            types: (list) has one or more of 'train', 'dev', 'test' depending on which data is required
            data_dir: (string) directory containing the dataset

        Returns:
            data: (dict) contains the data with labels for each type in types

        """
        data = {}
        
        for split in ['train', 'val', 'test']:
            if split in types:
                sets_file = os.path.join(data_dir, split, "sets.txt")
                labels_file = os.path.join(data_dir, split, "labels.txt")
                data[split] = {}
                self.load_sets_labels(sets_file, labels_file, data[split])

        return data

    def data_iterator(self, data, params, shuffle=False):
        """
        Returns a generator that yields batches data with labels. Batch size is params.batch_size. Expires after one
        pass over the data.

        Args:
            data: (dict) contains data which has keys 'data', 'labels' and 'size'
            params: (Params) hyperparameters of the training process.
            shuffle: (bool) whether the data should be shuffled

        Yields:
            batch_data: (Variable) dimension batch_size x seq_len with the sentence data
            batch_labels: (Variable) dimension batch_size x seq_len with the corresponding labels

        """

        # make a list that decides the order in which we go over the data- this avoids explicit shuffling of data
        order = list(range(data['size']))
        if shuffle:
            random.seed(230)
            random.shuffle(order)

        # one pass over data
        for i in range((data['size']+1)//params.batch_size):
            # fetch sentences and tags
            batch_sets = [data['data'][idx] for idx in order[i*params.batch_size:(i+1)*params.batch_size]]
            batch_labels = [data['labels'][idx] for idx in order[i*params.batch_size:(i+1)*params.batch_size]]
            batch_sizes = [len(data['data'][idx]) for idx in order[i*params.batch_size:(i+1)*params.batch_size]]

            # since all data are indices, we convert them to torch LongTensors
            print(i)
            batch_max_len = max([len(s) for s in batch_sets])

            listofzeros = list()
            for i in range(params.input_size) :
                listofzeros.append(0.)

            for i in range(len(batch_sets)) :
                if len(batch_sets[i]) < batch_max_len :
                    for j in range(batch_max_len - len(batch_sets[i])):
                        batch_sets[i].append(listofzeros)

            batch_sets, batch_labels = torch.FloatTensor(batch_sets), torch.FloatTensor(batch_labels)

            # shift tensors to GPU if available
            if params.cuda:
                batch_sets, batch_labels = batch_sets.cuda(), batch_labels.cuda()

            # convert them to Variables to record operations in the computational graph
            batch_sets, batch_labels = Variable(batch_sets), Variable(batch_labels)
    
            yield batch_sets, batch_labels, batch_sizes
